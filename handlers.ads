with Ada.Containers.Vectors;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
with GNAT.Sockets; use GNAT.Sockets;
with Constants; use Constants;
with Ada.Calendar; use Ada.Calendar;
with Ada.Streams; use Ada.Streams;
with Ada.Unchecked_Conversion;
with Ada.Strings.Fixed;
with Ada.Directories; use Ada.Directories;
with Ada.Direct_IO;
with Ada.Exceptions; use Ada.Exceptions;
with Ada.IO_Exceptions; use Ada.IO_Exceptions;

package handlers is

    type Client_Type is record
        socket      : Socket_Type;
        read        : Unbounded_String;
        unread      : Unbounded_String;
        last_active : Time;
    end record;

    type Client_Index_Type is range 1..100;
    package Client_Vector_Type is new Ada.Containers.Vectors (
        Element_Type => Client_Type,
        Index_Type   => Client_Index_Type);

    task type handler is
        entry start(id: Integer);
        entry handle(s: Socket_Type);
        entry command(s: Socket_Type);
        entry stop;
    end handler;

end handlers;
