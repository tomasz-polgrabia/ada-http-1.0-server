with Ada.Interrupts; use Ada.Interrupts;
with Ada.Interrupts.Names; use Ada.Interrupts.Names;
with Ada.Text_IO; use Ada.Text_IO;
with Gnat.Sockets; use Gnat.Sockets;

package signals is

    pragma Unreserve_All_Interrupts;

    type Selector_Access is access all Gnat.Sockets.Selector_Type;

    protected type Sigint_Handler is
        procedure selector(s: Selector_Access);
        procedure Handle;

        pragma Interrupt_Handler(Handle);
        pragma Attach_Handler(Handle, Sigint);
        private
        Call_Count : Natural := 0;
        sel        : Selector_Access;
    end Sigint_Handler;

end signals;
