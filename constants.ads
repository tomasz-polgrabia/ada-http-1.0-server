package constants is

    cr      : Character := Character'Val(13);
    lf      : Character := Character'Val(10);
    newline: String := (cr, lf);

end constants;
