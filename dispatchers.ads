with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
with Gnat.Sockets; use Gnat.Sockets;
with Ada.Calendar; use Ada.Calendar;
with Ada.Containers.Vectors;
with Handlers; use Handlers;

package dispatchers is
    task dispatcher is
        entry start;
        -- signal to start
        entry dispatch(s: Socket_Type);
        -- we got accepted socket from the listener
        entry stop;
        -- signal to stop
    end dispatcher;
end dispatchers;
