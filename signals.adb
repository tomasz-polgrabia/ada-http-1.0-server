package body signals is
    protected body Sigint_Handler is

        procedure selector(s: Selector_Access) is
        begin
            sel := s;
        end;

        procedure Handle is
        begin
            Call_Count := Call_Count + 1;
            Put_Line("SIGINT handled");
            Gnat.Sockets.Abort_Selector(sel.all);
        end Handle;
    end Sigint_Handler;
end signals;
